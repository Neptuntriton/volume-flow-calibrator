/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Some static Methods to use the Debug class

*********************************************************************************/

#include "Deb.h"

// global Var
cDebug *Deb = NULL;

void cDeb::Init(int ADebugLevel, QListView *AListView){
    if (Deb == NULL){
        Deb = new cDebug(ADebugLevel);
        Deb->ShowYouInThisListView(AListView);
    }
}

void cDeb::Low(QString Message){
    if (Deb != NULL){
        Deb->Low(Message);
    }
}

void cDeb::Med(QString Message){
    if (Deb != NULL){
        Deb->Med(Message);
    }
}

void cDeb::High(QString Message){
    if (Deb != NULL){
        Deb->High(Message);
    }
}
