/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : A Simple runtime debugging class

*********************************************************************************/

#ifndef DEBUG_H
#define DEBUG_H

#include <QObject>
#include <QDateTime>
#include <QListView>
#include <QStandardItemModel>

class cDebug :public QObject{
    Q_OBJECT

    private:
        QStandardItemModel      *MyDebugMessages;
        int                      MyDebugLevel;

    public:
        cDebug(int ADebugLevel);
        ~cDebug();
        void ShowYouInThisListView(QListView *AListView);
        void Low(QString Message);
        void Med(QString Message);
        void High(QString Message);
};

#endif

