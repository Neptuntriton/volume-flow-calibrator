/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Some static Methods to use the Debug class

*********************************************************************************/

#ifndef DEB_H
#define DEB_H

#include "Debug.h"

class cDeb {

    public:
        static void Init(int ADebugLevel, QListView *AListView);
        static void Low(QString Message);
        static void Med(QString Message);
        static void High(QString Message);

};

#endif
