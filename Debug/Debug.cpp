/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : A Simple runtime debugging class

*********************************************************************************/

#include "Debug.h"

cDebug::cDebug(int ADebugLevel){
    MyDebugLevel     = ADebugLevel;
    MyDebugMessages  = new QStandardItemModel();
}


cDebug::~cDebug(){
    delete MyDebugMessages;
}


void cDebug::Low(QString Message){
    if (MyDebugLevel >= 2){
        MyDebugMessages->appendRow(new QStandardItem(QDateTime::currentDateTime().toString() + ": " + Message));
    }
}

void cDebug::Med(QString Message){
    if (MyDebugLevel >= 1){
        MyDebugMessages->appendRow(new QStandardItem(QDateTime::currentDateTime().toString() + ": " + Message));
    }
}

void cDebug::High(QString Message){
    if (MyDebugLevel >= 0){
        MyDebugMessages->appendRow(new QStandardItem(QDateTime::currentDateTime().toString() + ": " + Message));
    }
}

void cDebug::ShowYouInThisListView(QListView *AListView){
    AListView->setModel(MyDebugMessages);
}


// globale Instanz des DebugFensters
//cDebug *Deb = NULL;



