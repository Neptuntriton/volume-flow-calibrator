#ifndef MAINGUI_H
#define MAINGUI_H


#include <QFile>               // Files
#include <QMainWindow>
#include <QtXml/QDomNode>      // XML-Node
#include <QtXml/QDomDocument>  // XML-Document
#include <Modbus/ModbusClient.h>
#include <Modbus/ModbusManager.h>
#include <Debug/Deb.h>



namespace Ui {
    class MainGUI;
}

class MainGUI : public QMainWindow {
    Q_OBJECT
    

    
    private:
        Ui::MainGUI    *ui;
        QDomDocument   *MyConfigFile;
        QDomNode       *MyEthSettings;
        QDomNode       *MyBalanceSettings;
        cModbusClient  *MyModBusClient;
        cModbusManager *MyModBusManager;

        void           ReadFromConfigFile();  // Wir lesen die Einstellungen


    private slots:
        void           ReactOnPushButtonClick();
        void           ReactOnHoleAInletTemp(cModbusMessage* AModbusMessage);
        void           ReactOnHoleBInletTemp(cModbusMessage* AModbusMessage);
        void           ReactOnHoleCInletTemp(cModbusMessage* AModbusMessage);
        void           ReactOnHoleAOutletTemp(cModbusMessage* AModbusMessage);
        void           ReactOnHoleBOutletTemp(cModbusMessage* AModbusMessage);
        void           ReactOnHoleCOutletTemp(cModbusMessage* AModbusMessage);

    public:
        explicit      MainGUI(QWidget *parent = 0);
                     ~MainGUI();
};

#endif // MAINGUI_H
