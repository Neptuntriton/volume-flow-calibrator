/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Class to read from Modbus Devices

*********************************************************************************/

#ifndef MODBUSCLIENT_H
#define MODBUSCLIENT_H

#include <QList>
#include <QTimer>
#include <QObject>
#include <QDateTime>
#include <QListView>
#include <QMetaEnum>
#include <QTcpSocket>
#include <QByteArray>
#include <./Debug/Deb.h>
#include <QStandardItemModel>

//#include <QHostAddress>
typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned char  TModBusFunctionCode;

// When no awnser is arrived in this interval the message is deleted
const int  MessageLifeTimeInMillis = 5000;
const int  ModBusStandardPort      = 502;
const byte ModBusStandardUnitID    = 0xFF;

// Define constants for the ModBus functions
// copied from ModbusConsts
const byte mbfReadCoils            = 0x01;
const byte mbfReadInputBits        = 0x02;
const byte mbfReadHoldingRegs      = 0x03;
const byte mbfReadInputRegs        = 0x04;
const byte mbfWriteOneCoil         = 0x05;
const byte mbfWriteOneReg          = 0x06;
const byte mbfWriteCoils           = 0x0F;
const byte mbfWriteRegs            = 0x10;
const byte mbfReadFileRecord       = 0x14;
const byte mbfWriteFileRecord      = 0x15;
const byte mbfMaskWriteReg         = 0x16;
const byte mbfReadWriteRegs        = 0x17;
const byte mbfReadFiFoQueue        = 0x18;


// eine ModbusNachricht
class cModbusMessage :public QObject{
    Q_OBJECT

    private:
        byte                MyUnitID;                   // Das zu fragende Gerät weist diese UnitID auf
        bool                MyLocked;
        QDateTime           MySendTime;                 // Wann wurde gefragt
        QDateTime           MyAwnseredTime;             // Wann wurde geantwortet
        word                MyTransActionID;
        word                MyProtocolID;
        word                MyPayLoadByteCount;
        word                MyAddress;
        word                MyAwnserAddress;
        word                MyInputCount;
        TModBusFunctionCode MyFunctionCode;
        QTcpSocket         *MyRefToSocketToDeliver;     // hier müssen die Daten hingesendet werden
        bool                MyAwnsered;                 // wurde die Frage beantwortet
        QByteArray         *MyData;                     // Hier liegen die Daten drin vor
        qint64              MyDeliveredByteCount;       // wieviele Bytes wurden gesendet

        // private Methods
        void                Awnsered();
        QDateTime           GetApproximatedMeasureTime();
        qint64              GetLatencyInMillis();
        void                Lock();
        void                UnLock();
        byte                Hi(word Input);
        byte                Lo(word Input);
        void                SendYou();


    public:
        // Getter
        word    Address();
        QString DataAsHexString();
        int     DataAsInt();

        cModbusMessage(word ATransactionID, byte AnAddress, byte AUnitID, TModBusFunctionCode AFunctionCode, QTcpSocket *ATcpSocketToDeliver);
        ~cModbusMessage();

    friend class cModbusClient;
};


// Wir leiten von QObject ab
class cModbusClient :public QObject{
    Q_OBJECT

    private:
        QTcpSocket*              MySocket;
        QString                  MyHostIP;
        int                      MyHostPort;
        int                      MyUnitID;
        QTimer*                  MyDeleteUnansweredMessagesTimer;
        QList<cModbusMessage* >* MyModbusMessages;               // Typisierte Liste aller Nachrichten die versendet wurden
        word                     MyLastTransactionID;

        // private Methods Section
        word                     GetNewTransactionID();
        void                     SendModbusCommand(const TModBusFunctionCode AModBusFunction, byte AUnitID, const word ARegisterNumber, const word ABlockLength, QByteArray Data);
        void                     AnalyseMessage(QByteArray ReceivedData);
        cModbusMessage*          FindMessage(word AMessageIDToSearchFor);
        void                     CheckForEventsToExecute(cModbusMessage* AModbusMessage);
        void                     KillOldMessages();
        // cause in C++ we cant't call a constructor within a constructor we have to use this private function
        void                     InitModbusClient(QString HostIP, unsigned short HostPort, unsigned char UnitID);

    private slots:
        void          ReactOnDeleteUnansweredMessagesTimer();
        void          ReactOnSocketConnect();
        void          ReactOnSocketDisconnect();
        void          ReactOnSocketData();
        void          ReactOnSocketDataSend(qint64 Count);
        void          ReactOnSocketError(QAbstractSocket::SocketError ASocketError);

    public:
        // Getter
        QString HostIP();

        // Der Konstructoren
        cModbusClient(QString HostIP);
        cModbusClient(QString HostIP, unsigned short HostPort);
        cModbusClient(QString HostIP, unsigned short HostPort, unsigned char UnitID);

        void Connect();
        void Disconnect();

        void TryToReadInPutRegister(const word AnInputRegisterNumber);
        void TryToReadInPutRegisters(const word AnInputRegisterNumber,const word Blocks);

        // Der Destructor
        ~cModbusClient();

    signals:
        void OnConnect();
        void OnDisconnect();
        void OnNewModBusMessage(cModbusMessage* AModbusMessage);
};

#endif
