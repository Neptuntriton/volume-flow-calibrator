/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Class to Manage multiple Modbus Devices in one application

*********************************************************************************/



#include <Modbus/ModbusClient.h>

#ifndef CINPUTREGISTERHANDLER_H
#define CINPUTREGISTERHANDLER_H

class cInputRegisterHandler :public QObject{
    Q_OBJECT

    private:
        word     MyRegister;


    signals:
        void     OnNewModBusMessage(cModbusMessage*);

    public:
        const char* HandlerName;
        QObject   * Reciever;
        word Register();
        cInputRegisterHandler(word AnInputRegisterToAsk);

    friend class cModbusEntity;
};

#endif //CINPUTREGISTERHANDLER_H

#ifndef CMODBUSENTITY_H
#define CMODBUSENTITY_H

// Repräsentiert ein Endgerät verbunden durch einen Client
class cModbusEntity :public QObject{
    Q_OBJECT

    private:
        QTimer*                         MyAskTimer;
        QList<cInputRegisterHandler* >* MyRegistersToAsk;
        cModbusClient*                  MyModbusClient;
        int                             MyPollIntervallInMillis;
        int                             MyLastAskedRegisterIndex;

        void Init(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis);

    private slots:
        void ReactOnAskTimer();
        void ReactOnNewModbusMessage(cModbusMessage* AModbusMessage);

    public:
        cModbusEntity(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis);
        cModbusEntity(QString HostIP, int HostPort, unsigned int PollIntervallInMillis);
        cModbusEntity(QString HostIP, unsigned int PollIntervallInMillis);
        ~cModbusEntity();
        void AddInputRegisterToScan(word AnInputRegisterToScan);
        void AddInputRegisterToScan(word AnInputRegisterToScan, QObject* AReciever, const char* AnOnNewModbusMessageHandlerSlot);

    signals:
        void OnNewModBusMessage(cModbusMessage* AModbusMessage);

};

#endif //CMODBUSENTITY_H

#ifndef CMODBUSMANAGER_H
#define CMODBUSMANAGER_H

// Wir leiten von QObject ab
class cModbusManager :public QObject{
    Q_OBJECT

    private:
        QList<cModbusEntity* >*  ModbusEntitys;    // Liste aller Entitäten


    private slots:
        void                     ReactOnNewModbusEntityMessage(cModbusMessage* AModbusMessage);


    public:
        // Getter
        cModbusEntity*           ModbusEntity(int Index);

        // Init and so
        cModbusManager();
        ~cModbusManager();
        cModbusEntity* AddModBusHost(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis);
        cModbusEntity* AddModBusHost(QString HostIP, int HostPort, unsigned int PollIntervallInMillis);
        cModbusEntity* AddModBusHost(QString HostIP, unsigned int PollIntervallInMillis);

  signals:
        void                     OnNewModBusMessage(cModbusMessage* AModbusMessage);

};


#endif // CMODBUSMANAGER_H
