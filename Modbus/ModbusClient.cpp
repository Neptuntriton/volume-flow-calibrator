/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Class to read from Modbus Devices

*********************************************************************************/

#include "ModbusClient.h"


//*******************************************************
// cModbusMessage
//*******************************************************

// Constructor
cModbusMessage::cModbusMessage(word ATransactionID, byte AnAddress, byte AUnitID, TModBusFunctionCode AFunctionCode, QTcpSocket *ATcpSocketToDeliver){
    MyUnitID               = AUnitID;
    MyLocked               = false;
    MyDeliveredByteCount   = 0;
    MyTransActionID        = ATransactionID;
    MyProtocolID           = 0x0000;
    MyPayLoadByteCount     = 1;
    MyAddress              = AnAddress;
    MyAwnserAddress        = 0;
    MyFunctionCode         = AFunctionCode;
    MyInputCount           = 01;
    MyRefToSocketToDeliver = ATcpSocketToDeliver;    
    MyData                 = new QByteArray();
}


// Destructor
cModbusMessage::~cModbusMessage(){
    delete MyData;
}


void cModbusMessage::Awnsered(){
    MyAwnseredTime = QDateTime::currentDateTime();
    MyAwnsered     = true;
}


QDateTime cModbusMessage::GetApproximatedMeasureTime(){
    if (MyAwnseredTime > MySendTime){
        return QDateTime::fromMSecsSinceEpoch((MySendTime.toMSecsSinceEpoch() + MyAwnseredTime.toMSecsSinceEpoch()) / 2);
    } else {
        return MySendTime;
    }
}


qint64 cModbusMessage::GetLatencyInMillis(){
    return MyAwnseredTime.toMSecsSinceEpoch() - MySendTime.toMSecsSinceEpoch();
}


void cModbusMessage::Lock(){
    MyLocked = true;
}


byte cModbusMessage::Hi(word Input){
    return Input >> 8;
}


byte cModbusMessage::Lo(word Input){
    return Input & 0xFF;
}

void cModbusMessage::SendYou(){
    if ((MyRefToSocketToDeliver != NULL) && (MyRefToSocketToDeliver->isOpen())){
        QByteArray SendBuffer;
        SendBuffer.append(Hi(MyTransActionID));
        SendBuffer.append(Lo(MyTransActionID));
        SendBuffer.append(Hi(MyProtocolID));
        SendBuffer.append(Lo(MyProtocolID));
        // zunächst ist die Länge des zu senden Telegrams noch unbekannt
        SendBuffer.append(char(0x00));
        SendBuffer.append(char(0x00));
        SendBuffer.append(MyUnitID);
        SendBuffer.append(MyFunctionCode);
        SendBuffer.append(Hi(MyAddress));
        SendBuffer.append(Lo(MyAddress));

        // was zur Hölle heißt das?
        SendBuffer.append(Hi(MyInputCount));
        SendBuffer.append(Lo(MyInputCount));
        SendBuffer.append(*MyData);
        //for a := 0 to length(MyData) -1 do begin
        //  SendBuffer := SendBuffer + Chr(MyData[a]);
        //end;

        //nun schreiben wir die Größe
        word len = SendBuffer.length() - 6;
        SendBuffer[4] = Hi(len);
        SendBuffer[5] = Lo(len);

        //We have to build our own Debugger
        cDeb::Low("Try to Send");
        cDeb::Low("|-> :" + QString(SendBuffer.toHex()));

        MySendTime           = QDateTime::currentDateTime();
        MyDeliveredByteCount = MyRefToSocketToDeliver->write(SendBuffer);
    }
}

void cModbusMessage::UnLock(){
    MyLocked = false;
}


word cModbusMessage::Address(){
    return MyAddress;
}

QString cModbusMessage::DataAsHexString(){
    return "0x" + MyData->toHex();
}

int cModbusMessage::DataAsInt(){
    int Result = 0x00;
    int Index  = MyData->count() -1;
    int Count  = 0;
    while ((Index > -1) && (Count < 5)){
        byte aByte = MyData->at(Index);
        //aByte <<= Count * 8;
        Result |= (aByte << Count * 8);
        Count += 1;
        Index -= 1;
    }
    return Result;
}


//*******************************************************
// cModbusClient
//*******************************************************

// Constructor
// All Stuff has to be initiated here
cModbusClient::cModbusClient(QString HostIP){
    cModbusClient(HostIP, 502, 0xFF);
}

cModbusClient::cModbusClient(QString HostIP, unsigned short HostPort){
    InitModbusClient(HostIP, HostPort, 0xFF);
}

cModbusClient::cModbusClient(QString HostIP, unsigned short HostPort, unsigned char UnitID){
    InitModbusClient(HostIP, HostPort, UnitID);
}

void cModbusClient::InitModbusClient(QString HostIP, unsigned short HostPort, unsigned char UnitID){
    MyHostIP                        = HostIP;
    MyHostPort                      = HostPort;
    MyUnitID                        = UnitID;
    MyLastTransactionID             = 0;

    MyDeleteUnansweredMessagesTimer = new QTimer();
    MyModbusMessages                = new QList<cModbusMessage *>();

    // Wir instanzieren das Socket
    MySocket                        = new QTcpSocket();

    // Wir verbinden die Handler
    QObject::connect(MyDeleteUnansweredMessagesTimer, SIGNAL(timeout()),    this, SLOT(ReactOnDeleteUnansweredMessagesTimer()));
    QObject::connect(MySocket, SIGNAL(connected()),                         this, SLOT(ReactOnSocketConnect()));
    QObject::connect(MySocket, SIGNAL(connectionClosed()),                  this, SLOT(ReactOnSocketDisconnect()));
    QObject::connect(MySocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ReactOnSocketError(QAbstractSocket::SocketError)));
    QObject::connect(MySocket, SIGNAL(readyRead()),                         this, SLOT(ReactOnSocketData()));
    QObject::connect(MySocket, SIGNAL(bytesWritten(qint64)),                this, SLOT(ReactOnSocketDataSend(qint64)));

    MyDeleteUnansweredMessagesTimer->start(MessageLifeTimeInMillis);
}


QString cModbusClient::HostIP(){
    return MyHostIP;
}


void cModbusClient::Connect(){
    // Wir öffnen das Socket
    cDeb::Med("Try to open Connection to Modbus Server!");
    cDeb::Med("|-> IP: " + MyHostIP);
    cDeb::Med("|-> Port: " + QString("%1").arg(MyHostPort));

    MySocket->connectToHost(MyHostIP, MyHostPort, QIODevice::ReadWrite);
}


void cModbusClient::Disconnect(){
    cDeb::Med("Disconnect from Modbus Server!");
    MySocket->close();
}


// Destructor
cModbusClient::~cModbusClient(){
    cModbusMessage *AModbusMessage;

    MyDeleteUnansweredMessagesTimer->stop();
    delete MyDeleteUnansweredMessagesTimer;

    // zunächst schließen wir das Socket
    MySocket->close();
    delete MySocket;    

    for (int i; i < MyModbusMessages->count(); i++){
        AModbusMessage = MyModbusMessages->at(i);
        delete AModbusMessage;
    }
    delete MyModbusMessages;
}


void cModbusClient::KillOldMessages(){
    cDeb::Low("cModbusClient::KillOldMessages");
    int             a     = 0;
    QDateTime       Now   = QDateTime::currentDateTime();
    cModbusMessage* AModbusMessage;
    while (a < MyModbusMessages->count() - 1){
        AModbusMessage = MyModbusMessages->at(a);
        if ((!AModbusMessage->MyLocked) && (AModbusMessage->MySendTime.toMSecsSinceEpoch() + MessageLifeTimeInMillis > Now.toMSecsSinceEpoch())){
            MyModbusMessages->removeAt(a);
            delete AModbusMessage;
        } else {
            a += 1;
        }
    }
}


void cModbusClient::AnalyseMessage(QByteArray ReceivedData){

    cDeb::Low("Analysing Data");

    int ALength = ReceivedData.length();
    if (ALength > 1) {
        word             AMessageID     = ((0xFF && ReceivedData[0]) << 8) + ReceivedData[1];
        cModbusMessage*  AModbusMessage = FindMessage(AMessageID);
        if (AModbusMessage != NULL){
            // dann blockieren wir die Nachricht demit sie vom Timer nicht erwischt wird
            AModbusMessage->Lock();

            if (ALength > 7){
                word                ADataCount    = ((0xFF && ReceivedData[4]) << 8) + ReceivedData[5];
                TModBusFunctionCode AFunctionCode = ReceivedData[7];
                if ((ALength == 6 + ADataCount) && (AModbusMessage->MyFunctionCode == AFunctionCode)) {
                    AModbusMessage->Awnsered();
                    if (ADataCount > 2){
                        word ARealDataByteCount = ReceivedData[8];
                        AModbusMessage->MyData->clear();
                        AModbusMessage->MyData->append(ReceivedData.mid(9,ARealDataByteCount));
                    }
                    CheckForEventsToExecute(AModbusMessage);
                }
            }

            // hier müssen wir die Nachricht nun definitv löschen
            // -> zunächst aus der Liste -> dann sich selbst
            MyModbusMessages->removeOne(AModbusMessage);
            delete AModbusMessage;

            // Der Timer darf nun wieder ran.
            AModbusMessage->UnLock();
        } else {
            cDeb::Low("No Question to Awnser with MessageID " + QString("%1").arg(AMessageID));
        }
    }
}

// Wir sichhen die Nachricht der Liste
cModbusMessage* cModbusClient::FindMessage(word AMessageIDToSearchFor){
    cModbusMessage* AMessage;
    for (int i = 0; i < MyModbusMessages->count(); i++){
        AMessage = MyModbusMessages->at(i);
        if (AMessage->MyTransActionID == AMessageIDToSearchFor){
            return AMessage;
        }
    }
    return NULL;
}


void cModbusClient::CheckForEventsToExecute(cModbusMessage* AModbusMessage){
    cDeb::Low("cModbusClient::CheckForEventsToExecute");
    emit OnNewModBusMessage(AModbusMessage);
}


void cModbusClient::ReactOnDeleteUnansweredMessagesTimer(){
    KillOldMessages();
}


void cModbusClient::ReactOnSocketConnect() {
    cDeb::Med("cModbusClient::Connected");
    OnConnect();
}


void cModbusClient::ReactOnSocketDisconnect(){
    cDeb::Med("cModbusClient::Disconnected");
    OnDisconnect();
}


void cModbusClient::ReactOnSocketData(){
    QByteArray ReciveBuffer = MySocket->readAll();
    cDeb::Low("Got Data");
    cDeb::Low("|-> Hex: " + ReciveBuffer.toHex());
    AnalyseMessage(ReciveBuffer);
}


void cModbusClient::ReactOnSocketDataSend(qint64 Count){
    cDeb::Low(QString("%1").arg(Count) + " Bytes send!");
}


void cModbusClient::ReactOnSocketError(QAbstractSocket::SocketError ASocketError){
    // We show the Text Interpretaion of the ENUM DataType QAbstractSocket::SocketError
    const QMetaObject &mo      = QAbstractSocket::staticMetaObject;
    QMetaEnum         metaEnum = mo.enumerator(mo.indexOfEnumerator("SocketError"));
    cDeb::Med("An Error occured: " + QString(metaEnum.valueToKey(ASocketError)) + " - " + MySocket->errorString());
}


word cModbusClient::GetNewTransactionID(){
    MyLastTransactionID += 1;
    return MyLastTransactionID;
}


void cModbusClient::SendModbusCommand(const TModBusFunctionCode AModBusFunction, byte AUnitID, const word ARegisterNumber, const word ABlockLength, QByteArray Data){
    switch (AModBusFunction){
        case mbfReadCoils   : {

        }
        case mbfReadInputBits   : {

        }
        case mbfReadHoldingRegs   : {

        }
        case mbfReadInputRegs   : {
        cModbusMessage *AModBusMessage = new cModbusMessage(GetNewTransactionID(), ARegisterNumber, AUnitID, AModBusFunction, MySocket);
            MyModbusMessages->append(AModBusMessage);
            AModBusMessage->SendYou();
        }
        case mbfWriteOneCoil   : {

        }
        case mbfWriteOneReg   : {

        }
        case mbfWriteCoils   : {

        }
        case mbfWriteRegs   : {

        }
        case mbfReadFileRecord   : {

        }
        case mbfWriteFileRecord   : {

        }
        case mbfMaskWriteReg   : {

        }
        case mbfReadWriteRegs   : {

        }
        case mbfReadFiFoQueue   : {

        }
    }
}


void cModbusClient::TryToReadInPutRegister(const word AnInputRegisterNumber){
    // Wir lesen nur ein Register mit der mehrere Register lesen Funktion
    TryToReadInPutRegisters(AnInputRegisterNumber, 1);
}


void cModbusClient::TryToReadInPutRegisters(const word AnInputRegisterNumber, const word Blocks){
    if (MySocket->isOpen()){
        QByteArray SomeData;
        SendModbusCommand(mbfReadInputRegs, MyUnitID, AnInputRegisterNumber, Blocks, SomeData);
    };
}
