/********************************************************************************
    Copyright (C) 2012  Jens Haupt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    ContactInfo    : Jens.Haupt.eu@googlemail.com
    Description    : Class to Manage multiple Modbus Devices in one application

*********************************************************************************/

#include <Modbus/ModbusManager.h>

// *******************************
// cInputRegisterHandler
// *******************************

cInputRegisterHandler::cInputRegisterHandler(word AnInputRegisterToAsk){
    MyRegister = AnInputRegisterToAsk;
}


word cInputRegisterHandler::Register(){
    return MyRegister;
}



// *******************************
// cModbusEntity
// *******************************

void cModbusEntity::Init(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis){
    MyLastAskedRegisterIndex = 0;

    // Wir bauen die RegisterLeseListe
    MyRegistersToAsk = new QList<cInputRegisterHandler*>;

    // Wir bauen den Client
    MyModbusClient = new cModbusClient(HostIP, HostPort, UnitID);
    // Wir vebinden sein Signal mit unserem Handler
    QObject::connect(MyModbusClient, SIGNAL(OnNewModBusMessage(cModbusMessage*)), this, SLOT(ReactOnNewModbusMessage(cModbusMessage*)));
    // Wir verbinden!
    MyModbusClient->Connect();

    // wir bauen den Timer
    MyAskTimer     = new QTimer;
    // Wir verbinden den Timer mit seiner auszulösenden Funnktion
    QObject::connect(MyAskTimer, SIGNAL(timeout()), this, SLOT(ReactOnAskTimer()));
    // Wir starten den Timer
    MyAskTimer->start(PollIntervallInMillis);
}


cModbusEntity::cModbusEntity(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis){
    Init(HostIP, HostPort, UnitID, PollIntervallInMillis);
}


cModbusEntity::cModbusEntity(QString HostIP, int HostPort, unsigned int PollIntervallInMillis){
    Init(HostIP, HostPort, ModBusStandardUnitID, PollIntervallInMillis);
}


cModbusEntity::cModbusEntity(QString HostIP, unsigned int PollIntervallInMillis){
    Init(HostIP, ModBusStandardPort, ModBusStandardUnitID, PollIntervallInMillis);
}


// Wenn der Timer anschlägt gehen wir die Liste der zu fragenden Register durch und Fragen eins nach dem anderen ab
void cModbusEntity::ReactOnAskTimer(){
    if (MyRegistersToAsk->count() > 0){
        int Index = MyLastAskedRegisterIndex % MyRegistersToAsk->count();
        cDeb::Low("cModbusEntity - Try to ask Register.");
        cDeb::Low(" |-> Index: " + QString("%1").arg(Index));
        cDeb::Low(" |-> Register: " + QString("%1").arg(MyRegistersToAsk->at(Index)->Register()));

        MyModbusClient->TryToReadInPutRegister(MyRegistersToAsk->at(Index)->Register());

        MyLastAskedRegisterIndex += 1;
    } else {
        cDeb::Low("cModbusEntity - No Register to ask.");
    }
}


void  cModbusEntity::ReactOnNewModbusMessage(cModbusMessage* AModbusMessage){
    // Falls ein generisches Event eingehangen ist muss dieses hier ausgelöst werden
    emit OnNewModBusMessage(AModbusMessage);

    // wir gehen die Liste durch und lösen die speziellen Signale aus
    for (int a = 0; a < MyRegistersToAsk->count(); a++){
        if (AModbusMessage->Address() == MyRegistersToAsk->at(a)->Register()){
            emit MyRegistersToAsk->at(a)->OnNewModBusMessage(AModbusMessage);
        }
    }
}


// wir fügen das zu scannende Register hinzu
void cModbusEntity::AddInputRegisterToScan(word AnInputRegisterToScan){
    AddInputRegisterToScan(AnInputRegisterToScan, NULL, "");
}


void cModbusEntity::AddInputRegisterToScan(word AnInputRegisterToScan, QObject* AReciever, const char* AnOnNewModbusMessageHandlerSlot){
    //void cModbusEntity::AddInputRegisterToScan(word AnInputRegisterToScan, QString AModBusMessageHandler){
    cInputRegisterHandler* AnInputRegisterHandler = new cInputRegisterHandler(AnInputRegisterToScan);
    AnInputRegisterHandler->Reciever = AReciever;
    AnInputRegisterHandler->HandlerName = AnOnNewModbusMessageHandlerSlot;

    if (AReciever != NULL) {
        QObject::connect(AnInputRegisterHandler, SIGNAL(OnNewModBusMessage(cModbusMessage*)), AReciever, AnOnNewModbusMessageHandlerSlot);
    }

    MyRegistersToAsk->append(AnInputRegisterHandler);
}


cModbusEntity::~cModbusEntity(){
    cDeb::Med("cModbusEntity disconnects from " + MyModbusClient->HostIP());
    // Wir stoppen den Timer
    MyAskTimer->stop();
    // wir löschen ihn
    delete MyAskTimer;

    // wir löschen die Liste
    delete MyRegistersToAsk;

    // wir löschen den ModBusClient
    MyModbusClient->Disconnect();
    delete MyModbusClient;
}



// *******************************
// cModbusManager
// *******************************


// wir erstellen die Liste zur Aufnahme der Hosts
cModbusManager::cModbusManager(){
    ModbusEntitys = new QList<cModbusEntity*>;
}


// wir reagieren auf Nachrichten der Hosts
void cModbusManager::ReactOnNewModbusEntityMessage(cModbusMessage* AModbusMessage){
    cDeb::Low("cModbusManager - New Message");
    cDeb::Low("|-> Register: " + QString("%1").arg(AModbusMessage->Address()));
    cDeb::Low("|-> Data: " + AModbusMessage->DataAsHexString());
}


// wir löschen alles wieder aus dem Speicher
cModbusManager::~cModbusManager(){
    cModbusEntity* AModbusEntity;
    for (int i = 0; i < ModbusEntitys->count(); i++){
        AModbusEntity = ModbusEntitys->at(i);
        delete AModbusEntity;
    }
    ModbusEntitys->clear();
    delete ModbusEntitys;
}


cModbusEntity* cModbusManager::ModbusEntity(int Index){
    return ModbusEntitys->at(Index);
}


// Three Versions of Adding an ModBus Entity
cModbusEntity* cModbusManager::AddModBusHost(QString HostIP, int HostPort, byte UnitID, unsigned int PollIntervallInMillis){
    cModbusEntity* AModbusEntity = new cModbusEntity(HostIP, HostPort, UnitID, PollIntervallInMillis);
    QObject::connect(AModbusEntity, SIGNAL(OnNewModBusMessage(cModbusMessage*)), this, SLOT(ReactOnNewModbusEntityMessage(cModbusMessage*)));
    ModbusEntitys->append(AModbusEntity);
    return AModbusEntity;
}


cModbusEntity* cModbusManager::AddModBusHost(QString HostIP, int HostPort, unsigned int PollIntervallInMillis){
    cModbusEntity* AModbusEntity = new cModbusEntity(HostIP, HostPort, PollIntervallInMillis);
    QObject::connect(AModbusEntity, SIGNAL(OnNewModBusMessage(cModbusMessage*)), this, SLOT(ReactOnNewModbusEntityMessage(cModbusMessage*)));
    ModbusEntitys->append(AModbusEntity);
    return AModbusEntity;
}


cModbusEntity* cModbusManager::AddModBusHost(QString HostIP, unsigned int PollIntervallInMillis){
    cModbusEntity* AModbusEntity = new cModbusEntity(HostIP, PollIntervallInMillis);
    QObject::connect(AModbusEntity, SIGNAL(OnNewModBusMessage(cModbusMessage*)), this, SLOT(ReactOnNewModbusEntityMessage(cModbusMessage*)));
    ModbusEntitys->append(AModbusEntity);
    return AModbusEntity;
}
