// Was muss dieses Program können?
// * Einlesen der Wert der Waage
// * steuern und lesen der des Ethernet controllers über Modbus/TCPIP
// * RS232 mit dem Mini

#include "maingui.h"
#include "ui_maingui.h"


// Constructor
MainGUI::MainGUI(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainGUI){
    ui->setupUi(this);
    this->setWindowTitle("VolumeFlowCalibrator");

    cDeb::Init(1, ui->MessagesListView);
    cDeb::Med("VolumeFlowCalibrator started");

    QObject::connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(ReactOnPushButtonClick()));

    // Wir laden die Einstellungen
    ReadFromConfigFile();

    MyModBusManager = new cModbusManager;
    cModbusEntity* AnEntity;

    // Füllstand Kühlwasser
    //AnEntity = MyModBusManager->AddModBusHost("141.30.153.211", 1000);
    //AnEntity->AddInputRegisterToScan(01, this, SLOT(ReactOnHoleOneTemp(cModbusMessage*)));

    // Die Handler aller Sonden werden definiert und eingehangen
    // Alle Vorläufe
    AnEntity = MyModBusManager->AddModBusHost("141.30.153.215", 601, 1000);
    AnEntity->AddInputRegisterToScan(00, this, SLOT(ReactOnHoleAInletTemp(cModbusMessage*)));
    AnEntity->AddInputRegisterToScan(01, this, SLOT(ReactOnHoleBInletTemp(cModbusMessage*)));
    AnEntity->AddInputRegisterToScan(02, this, SLOT(ReactOnHoleCInletTemp(cModbusMessage*)));

    // Alle Rückläufe
    AnEntity = MyModBusManager->AddModBusHost("141.30.153.215", 600, 1000);
    AnEntity->AddInputRegisterToScan(00, this, SLOT(ReactOnHoleAOutletTemp(cModbusMessage*)));
    AnEntity->AddInputRegisterToScan(01, this, SLOT(ReactOnHoleBOutletTemp(cModbusMessage*)));
    AnEntity->AddInputRegisterToScan(02, this, SLOT(ReactOnHoleCOutletTemp(cModbusMessage*)));

}

// Vorlauf Sonde 1
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_2
// On 141.30.153.215:601 Kanal 00
void MainGUI::ReactOnHoleAInletTemp(cModbusMessage* AModbusMessage){
    ui->InletSonde1->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}

// Vorlauf Sonde 2
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_2
// On 141.30.153.215:601 Kanal 01
void MainGUI::ReactOnHoleBInletTemp(cModbusMessage* AModbusMessage){
    ui->InletSonde2->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}

// Vorlauf Sonde 3
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_2
// On 141.30.153.215:601 Kanal 02
void MainGUI::ReactOnHoleCInletTemp(cModbusMessage* AModbusMessage){
    ui->InletSonde3->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}


// Rücklauf Sonde 1
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_3
// On 141.30.153.215:600 Kanal 00
void MainGUI::ReactOnHoleAOutletTemp(cModbusMessage* AModbusMessage){
    ui->OutletSonde1->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}

// Rücklauf Sonde 2
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_3
// On 141.30.153.215:600 Kanal 01
void MainGUI::ReactOnHoleBOutletTemp(cModbusMessage* AModbusMessage){
    ui->OutletSonde2->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}

// Rücklauf Sonde 3
// http://wiki.iet.mw.tu-dresden.de/IETWiki/index.php/Wachendorff_ETHIO4PI_3
// On 141.30.153.215:600 Kanal 02
void MainGUI::ReactOnHoleCOutletTemp(cModbusMessage* AModbusMessage){
    ui->OutletSonde3->setText(QString::number(0.1 * AModbusMessage->DataAsInt()) + QString::fromUtf8(" °C"));
}


// Destructor
MainGUI::~MainGUI(){
    delete MyModBusClient;
    delete ui;
}


void MainGUI::ReactOnPushButtonClick(){
    if (MyModBusClient != NULL){
        cDeb::High("Button clicked");
        MyModBusClient->TryToReadInPutRegister(01);
    }
}

// Lesen des Setups
// Wir speichern die Referenzen des ETModule Nodes und des Balance Nodes ab
// Achtung um XML zu verwenden muss das in der Projektdatei eingetragen werden
// QT += core gui network xml
void MainGUI::ReadFromConfigFile(){
    QDomNode     ARootNode, SubNode;
    QString      AFileName     = "config.xml";
    QFile        *AFile        = new QFile(AFileName);
                  MyConfigFile = new QDomDocument("config");   // unsere Einstellungen
    if (AFile->exists() && AFile->open(QFile::ReadOnly)){
        if (MyConfigFile->setContent(AFile)){
            ARootNode = MyConfigFile->documentElement();
            if ((!ARootNode.isNull()) && (ARootNode.nodeName() == "config")) {
                for (int i = 0; i <= ARootNode.childNodes().count(); i++){
                    SubNode = ARootNode.childNodes().at(i);
                    if (!SubNode.isNull() && SubNode.nodeName() == "ETModule"){
                        MyEthSettings = &SubNode;
                    }
                    if (!SubNode.isNull() && SubNode.nodeName() == "Balance"){
                        MyBalanceSettings = &SubNode;
                        this->setWindowTitle("Balance Settings found");
                    }
                }
            }

        } else {
            this->setWindowTitle("config read error");
        }
        AFile->close();
    } else {
        this->setWindowTitle("config.xml does not exist");
    }
    delete AFile;
}
