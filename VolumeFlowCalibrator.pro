#-------------------------------------------------
#
# Project created by QtCreator 2012-12-17T15:56:16
#
#-------------------------------------------------

QT       += core gui network xml svg

TARGET   =  VolumeFlowCalibrator
TEMPLATE =  app


SOURCES +=  main.cpp\
            maingui.cpp \
            Modbus/ModbusClient.cpp \
            Debug/Debug.cpp \
            Debug/Deb.cpp \
            Modbus/ModbusManager.cpp

HEADERS  += maingui.h \
            Modbus/ModbusClient.h \
            Debug/Debug.h \
            Debug/Deb.h \
            Modbus/ModbusManager.h

FORMS    += maingui.ui
